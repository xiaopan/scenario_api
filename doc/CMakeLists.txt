################################################################################
# Copyright (c) 2022 Mercedes-Benz Tech Innovation GmbH
#
# This program and the accompanying materials are made available under the terms
# of the Eclipse Public License 2.0 which is available at
# https://www.eclipse.org/legal/epl-2.0/
#
# SPDX-License-Identifier: EPL-2.0
################################################################################

find_package(Doxygen QUIET REQUIRED dot OPTIONAL_COMPONENTS mscgen dia)

set(DOXYGEN_BUILTIN_STL_SUPPORT YES)
set(DOXYGEN_DOT_IMAGE_FORMAT svg)
set(DOXYGEN_IMAGE_PATH ${CMAKE_CURRENT_LIST_DIR}/images)
set(DOXYGEN_INCLUDE_PATH ${PROJECT_SOURCE_DIR}/include)
set(DOXYGEN_INTERACTIVE_SVG YES)
set(DOXYGEN_QUIET YES)
set(DOXYGEN_TAB_SIZE 2)
set(DOXYGEN_UML_LOOK YES)
set(DOXYGEN_USE_MDFILE_AS_MAINPAGE README.md)

doxygen_add_docs(
  ${PROJECT_NAME}_doc ${PROJECT_SOURCE_DIR}/README.md ${PROJECT_SOURCE_DIR}/include COMMENT "Generate html docs"
)
