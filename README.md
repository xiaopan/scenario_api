# MantleAPI

A collection of interfaces for abstraction between a scenario engine and an environment simulator.
It is intended to be usable with a wide variety of scenario description languages by implementing according scenario engines.

Remark: This is currently work in progress and no stable state is reached yet.

## Used libraries

### Units
License: MIT License
URL: https://github.com/nholthaus/units
Version: v2.3.3

### GoogleTest
License: BSD-3-Clause License
URL: https://github.com/google/googletest
Version: v1.12.1

### CPM
License: MIT License
URL: https://github.com/cpm-cmake/CPM.cmake
Version: v0.36.0


This is a test change
