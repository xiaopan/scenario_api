/*******************************************************************************
 * Copyright (c) 2021-2022, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

//-----------------------------------------------------------------------------
/** @file  i_entity.h */
//-----------------------------------------------------------------------------

#ifndef MANTLEAPI_TRAFFIC_I_ENTITY_H
#define MANTLEAPI_TRAFFIC_I_ENTITY_H

#include <MantleAPI/Common/bounding_box.h>
#include <MantleAPI/Common/i_identifiable.h>
#include <MantleAPI/Common/pose.h>
#include <MantleAPI/Traffic/entity_properties.h>
#include <MantleAPI/Traffic/i_controller_config.h>

#include <memory>
#include <vector>

namespace mantle_api
{
struct EntityVisibilityConfig
{
  /// The "graphics" flag determines, if the entity shall be shown in visualizations of the simulated environment
  bool graphics{true};
  /// The "traffic" flag determines, if the entity is contained in the common simulator output (e.g. OSI Ground Truth)
  /// for all traffic participants
  bool traffic{true};
  /// The "sensors" flag can be used to simulate sensor errors (false positives / false negatives). For this the
  /// "sensors" flag value has to diverge from the "traffic" flag value (e.g. false positive: traffic=false,
  /// sensors=true)
  bool sensors{true};
  /// - Sensor errors can be specified for single sensors only. E.g. a false positive for a radar sensor would be
  /// specified with traffic="false", sensors="true", sensor_names={"radar"}.
  /// - For the other not named sensors implicitly the opposite applies. So in the above example e.g. for a camera
  /// sensor the entity would be not visible / not contained in its OSI Sensor View.
  /// - When "sensor_names" is not specified (empty vector) then the "sensors" flag applies to
  /// all sensors.
  std::vector<std::string> sensor_names{};
};

/// Base interface for all static and dynamic scenario entities.
class IEntity : public IIdentifiable
{
public:
  /// The position of the entity is the geometric center of its bounding box. The origin of the entity coordinate system
  /// can be defined flexibly in relation to the geometric center (see bounding box).
  virtual void SetPosition(const Vec3<units::length::meter_t>& inert_pos) = 0;
  [[nodiscard]] virtual Vec3<units::length::meter_t> GetPosition() const = 0;

  virtual void SetVelocity(const Vec3<units::velocity::meters_per_second_t>& velocity) = 0;
  [[nodiscard]] virtual Vec3<units::velocity::meters_per_second_t> GetVelocity() const = 0;

  virtual void SetAcceleration(const Vec3<units::acceleration::meters_per_second_squared_t>& acceleration) = 0;
  [[nodiscard]] virtual Vec3<units::acceleration::meters_per_second_squared_t> GetAcceleration() const = 0;

  virtual void SetOrientation(const Orientation3<units::angle::radian_t>& orientation) = 0;
  [[nodiscard]] virtual Orientation3<units::angle::radian_t> GetOrientation() const = 0;

  virtual void SetOrientationRate(
      const Orientation3<units::angular_velocity::radians_per_second_t>& orientation_rate) = 0;
  [[nodiscard]] virtual Orientation3<units::angular_velocity::radians_per_second_t> GetOrientationRate() const = 0;

  virtual void SetOrientationAcceleration(
      const Orientation3<units::angular_acceleration::radians_per_second_squared_t>& orientation_acceleration) = 0;
  [[nodiscard]] virtual Orientation3<units::angular_acceleration::radians_per_second_squared_t> GetOrientationAcceleration()
      const = 0;

  virtual void SetProperties(std::unique_ptr<mantle_api::EntityProperties> properties) = 0;
  [[nodiscard]] virtual EntityProperties* GetProperties() const = 0;

  virtual void SetAssignedLaneIds(const std::vector<std::uint64_t>& assigned_lane_ids) = 0;
  [[nodiscard]] virtual std::vector<std::uint64_t> GetAssignedLaneIds() const = 0;

  virtual void SetVisibility(const EntityVisibilityConfig& visibility) = 0;
  [[nodiscard]] virtual EntityVisibilityConfig GetVisibility() const = 0;
};

class IVehicle : public virtual IEntity
{
public:
  [[nodiscard]] VehicleProperties* GetProperties() const override = 0;

  virtual void SetIndicatorState(IndicatorState state) = 0;
  [[nodiscard]] virtual IndicatorState GetIndicatorState() const = 0;

  //    virtual bool IsHost() const = 0;
  //    virtual void SetHost() = 0;
};

class IPedestrian : public virtual IEntity
{
public:
  [[nodiscard]] PedestrianProperties* GetProperties() const override = 0;
};

class IStaticObject : public virtual IEntity
{
public:
  [[nodiscard]] StaticObjectProperties* GetProperties() const override = 0;
};

}  // namespace mantle_api

#endif  // MANTLEAPI_TRAFFIC_I_ENTITY_H
